using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("Stop Panel")]
    [SerializeField] private GameObject StopPanel;

    private static UIManager instance;

    public static UIManager Instance()
    {
        return instance;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
    }

    public void OnJumpButtonClick()
    {
        GameManager.Instance().OnJumpButtonClick();
    }
    
    public void OnPlayButtonClick()
    {
        GameManager.Instance().StartPlay();
    }

    public void SetActiveStopPanel(bool pointer)
    {
        StopPanel.SetActive(pointer);
    }
}
