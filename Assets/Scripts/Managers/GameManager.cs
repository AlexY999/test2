using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    [Header("Player Settings")]
    [SerializeField] private GameObject playerGameObject;
    [SerializeField] private float gravityScaleValue = 1;
    [Header("Jump Settings")] 
    [SerializeField] private float jumpForce = 5f;
    [SerializeField] private float clickdelay = 0.1f;
    [Header("Spawner Settings")]
    [SerializeField] private float spawnRate = 1f;
    [SerializeField] private float preventionSpeed = 2f;
    [SerializeField] private Transform preventionSpawner;
    
    private bool isPlaying = false;
    private bool isGrounded = true;
    private bool isStuckCeiling = false;
    private Rigidbody2D playerRigidbody2D;
    private float clicked = 0;
    private float clicktime = 0;
    private Coroutine waitForSecondClick;
    private Coroutine spawnPreventionsCoroutine;
    private string preventionPrefabTag = "Prevention";
    private Vector2 preventionSpawnVector2 = new Vector2(12, 0);

    private static GameManager instance;

    public static GameManager Instance()
    {
        return instance;
    }

    public void SetIsGrounded(bool pointer)
    {
        isGrounded = pointer;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        playerRigidbody2D = playerGameObject.GetComponent<Rigidbody2D>();
        StartPlay();
    }

    public void StartPlay()
    {
        if (isStuckCeiling)
        {
            PlayerChangeGravity();
        }
        
        SetIsGPlaying(true);
        spawnPreventionsCoroutine = StartCoroutine(nameof(SpawnPreventions));
        UIManager.Instance().SetActiveStopPanel(false);
    }

    public void StopPlay()
    {
        SetIsGPlaying(false);
        if(spawnPreventionsCoroutine != null)
            StopCoroutine(spawnPreventionsCoroutine);
        UIManager.Instance().SetActiveStopPanel(true);
    }
    
    public void OnJumpButtonClick()
    {
        if (isStuckCeiling)
        {
            PlayerChangeGravity();
            return;
        }

        clicked++;
        
        if (clicked == 1)
        {
            clicktime = Time.time;
            PlayerJump();
        }
 
        if (clicked > 1 && Time.time - clicktime < clickdelay)
        {
            clicked = 0;
            clicktime = 0;
            PlayerChangeGravity();
            
            if(waitForSecondClick != null)
                StopCoroutine(waitForSecondClick);
        } 
        else if (clicked > 2 || Time.time - clicktime > clickdelay)
        {
            clicked = 1;
            clicktime = Time.time;
            PlayerJump();
        }
    }
    
    private void SetIsGPlaying(bool pointer)
    {
        isPlaying = pointer;
    }

    private void PlayerJump()
    {
        if (isGrounded)
        {
            float koef = 1;
            koef = playerRigidbody2D.gravityScale > 0 ? 1 : -1;
            koef *= jumpForce;
            playerRigidbody2D.AddForce(Vector2.up * koef, ForceMode2D.Impulse);
        }
    }

    private void PlayerChangeGravity()
    {
        isStuckCeiling = !isStuckCeiling;
        playerRigidbody2D.gravityScale = -1 * playerRigidbody2D.gravityScale * gravityScaleValue;
    }

    IEnumerator SpawnPreventions()
    {
        while (isPlaying)
        {
            GameObject obj = ObjectPool.SharedInstance.GetPooledObject(preventionPrefabTag, true, preventionSpawnVector2,
                Quaternion.identity, preventionSpawner);

            int koef = Random.Range(0, 2) == 0 ? -1 : 1;
            obj.GetComponent<Rigidbody2D>().gravityScale = gravityScaleValue * koef;

            obj.transform.localScale = new Vector3(Random.Range(1, 5), 0.5f * Random.Range(2, 4), 1);
            
            StartCoroutine(MovePreventions(obj));
            
            yield return new WaitForSeconds(spawnRate);
        }
    }

    IEnumerator MovePreventions(GameObject obj)
    {
        Renderer m_Renderer;
        m_Renderer = obj.GetComponent<Renderer>();

        while (obj.transform.position.x > -preventionSpawnVector2.x && isPlaying)
        {
            obj.transform.Translate(Vector3.left * Time.deltaTime * preventionSpeed);
            yield return null;
        }
        
        obj.SetActive(false);
    }
}
