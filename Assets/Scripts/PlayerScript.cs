using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private string groundTag = "Ground";
    private string preventionTag = "Prevention";
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag(groundTag))
            GameManager.Instance().SetIsGrounded(true);
        else if (collision.gameObject.CompareTag(preventionTag))
            GameManager.Instance().StopPlay();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag(groundTag))
            GameManager.Instance().SetIsGrounded(false);
    }
}
